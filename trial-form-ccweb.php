<?php
?>
<!doctype html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>	
<meta HTTP-EQUIV="content-type" content="text/html; charset=utf-8" />
<meta content="IE=edge,chrome=1" HTTP-EQUIV="X-UA-Compatible" />
<meta content="en-us" HTTP-EQUIV="content-Language" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />



<link REL="stylesheet" HREF="reset.css" type="text/css" MEDIA="screen"/>
<link REL="stylesheet" HREF="styles.css" type="text/css" MEDIA="screen"/>
<link REL="stylesheet" HREF="animate.css" type="text/css" />
<link REL="stylesheet" HREF="style.css" type="text/css" />
<link HREF="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,700" REL="stylesheet">
	
<script>document.addEventListener("touchstart", function() {},false);</script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>
<script src="jquery-1.11.1.js"></script>
<script src="jquery.validate.min.js"></script>


<!-- <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>  -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!-- <script src="typeahead.js"></script> -->
    <!-- <script src="typeahead-addresspicker.js"></script> -->


	<script>
jQuery(function($) {
    // globals
    var email;
    var phone_number;

    var signup_status;
    var reservationURL;
    var did;
    var scrm_id;
    var current_form;
    
    // prevent Enter button
    //$(document).keypress(function(e) {
      //if(e.which == 13) {
          //e.preventDefault();
          //console.log('Enter Key Pressed');
      //} 
    //});
    
    // ninjaLoading
    var ninjaJobs = [
        "WARMING UP WITH SOME NUNCHUCKS",
        "THROWING NINJA STARS",
        "SHARPENING SWORDS",
        "PREPARING NINJA BELT"
    ];

    var selected_number;

    // Click Event For Appended Elements
    $(document).on('click', '.number_option', function(){
        console.log("CLICKED");

        // Reset Button Colors
        $('.number_option').css({
            "background":"#f0f0f2",
            "color":"#363e5a"
        });

        // Set clicked button color
        $(this).css({
            "background":"#DB7062",
            "color":"white"
        });

        // Set selected number to currently selected number
        selected_number = $(this).html();

        console.log("Selected Number: "+selected_number);
    });


    
    $('#closeOverlay').on('click', function(){
        console.log("close overlay clicked");
        closeNav();
    });

    var i = 0;

    setInterval(function(){
  
    if(i >= ninjaJobs.length){
        i = 0;
    }
  
    // console.log(ninjaJobs[i]);
    $('.loadingMSG').html("ONE MOMENT PLEASE... " +ninjaJobs[i]);
    i++;  
  
    }, 2000);
    
    // Animated panels
    function showHidePanels(toHide, toShow, direction, animationIn, animationOut){
        console.log('Hide: '+toHide+", Show: "+toShow);
        current_form = toShow;

        //$('html,body,.overlay').scrollTop(0);
  
        if(direction == "next"){
            $('#'+toHide).hide();
            $('#'+toHide).removeClass(animationIn);  
            $('#'+toHide).addClass(animationOut);
          
            $('#'+toShow).css('display','block');
            $('#'+toShow).removeClass(animationIn); 
            $('#'+toShow).removeClass(animationOut); 
            $('#'+toShow).addClass('animated '+animationIn);  
        } else {
            $('#'+toHide).removeClass(animationIn);  
            $('#'+toHide).addClass(animationOut);
          
            $('#'+toShow).removeClass(animationOut); 
            $('#'+toShow).css('display','block');
            $('#'+toShow).addClass('animated '+animationIn);     
        }   
    }
    
 // $('#phone_number').mask("(000) 000-0000", {placeholder: "Must be a mobile number"}); 
//  $('#verify_code').mask("0000", {placeholder: "____"}); 
//  $('#zipcode').mask("00000", {placeholder: ""}); 
  
  
// BASICS FORM ********************************************************************************

$('#basics_form_next').on('click', function(e){
    e.preventDefault();
    showHidePanels("form_one", "form_two", "next", "bounceInRight", "bounceOutLeft");
     
     return;
  
    $("#basics_form").validate({
        errorLabelContainer: ".messageBox",
        rules: {
            first_name: { required: true },
            last_name: { required: true },
            email: {
                required: true,
                email: true
            }
        },
  messages: {
    first_name: "Please enter your first name.",
    last_name: "Please enter your last name.",
    email: {
      required: "We need your email address to contact you!",
      email: "Your email address must be in the format of name@domain.com."
    }
  },
  submitHandler: function() {
      
    // $('.preloader').css('display', 'block');  
    $('#loadingBlackout').fadeIn();
    // set post params to send to api   
      var first_name = $('#first_name').val();
      var last_name  = $('#last_name').val();
      var email      = $('#email').val();
     // var reference  = $('#how').val();
     // var company    = $('#company').val();
      var step       = 1;
      
      // email
      email = $('#email').val();
      
      $.post('https://ninjanumber.com/trial_checkout.php', {
          first_name: first_name,
          last_name: last_name,
          email: email,
         // reference: reference,
         // company: company,
          step: step
      }, function(response){
        var status = JSON.parse(response);
        // var status = response;
        console.log("RESPONSE?????: "+ status);  
        
        if(status['response'] == "success"){
            
            scrm_id = status['id'];
            
            $('#gtag-capture').html();
            $('#gtag-capture').html("<script>gtag('config', 'UA-99590284-2', {'page_path': '/free-trial-email.html'});<\/script> ");
            //captera conversion pixel
			$('#gtag-capture').html("<script>var capterra_vkey = 'd182dab0e82acefafcd4c22e988cbf45',capterra_vid = '2120724',capterra_prefix = (('https:' == document.location.protocol) ? 'https://ct.capterra.com' : 'http://ct.capterra.com');(function() {var ct = document.createElement('script'); ct.type = 'text/javascript'; ct.async = true; ct.src = capterra_prefix + '/capterra_tracker.js?vid=' + capterra_vid + '&vkey=' + capterra_vkey; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ct, s); })();<\/script>");
			
            // $('.preloader').css('display', 'none');
            $('#loadingBlackout').fadeOut();
            console.log('success');
            
            showHidePanels("form_one", "personal_form", "next", "bounceInRight", "bounceOutLeft");
            
        } else {
            
        }
        console.log("Sending data to API");
      });
    }
    });
}); 


// *** END BASIC ***************************************************************

$('#personal_form_back').on('click', function(e){
    e.preventDefault();
  showHidePanels("form_two", "form_one","back", "bounceInRight", "bounceOutLeft");   
});

$('#form_three_back').on('click', function(e){
    e.preventDefault();
  showHidePanels("form_three", "form_two","back", "bounceInRight", "bounceOutLeft");   
});

$('#done_back').on('click', function(e){
    e.preventDefault();
    showHidePanels("summary_form", "area_code_form","back", "bounceInRight", "bounceOutLeft");   
});

$('#personal_form_next').on('click', function(e){
   $('#overlay_header').html('');
   e.preventDefault();
    showHidePanels("form_two", "form_three", "next", "bounceInRight", "bounceOutLeft");
    
    $("#personal_form").validate({
        errorLabelContainer: ".messageBox",
        rules: {
            phone_number: { required: true },
            address: { required: true } ,
            city: { required: true },
            state: { required: true },
            zipcode: { required: true }
        },
        messages: {
            phone_number: "We need a phone number for validation.",
            address: "Please enter your address.",
            city: "Please enter your city.",
            zipcode: "Please enter your zip code."
        },
  submitHandler: function() {
    
      
    //   $('.preloader').css('display', 'block'); 
      $('#loadingBlackout').fadeIn();
      email = $('#email').val();
      console.log("EMAIL: "+email);
      
    // set post params to send to api
      $.post('https://ninjanumber.com/trial_checkout.php', {
        phone_number: $('#phone_number').val(),
        address: $('#address').val(),
        address2: $('#address2').val(),
        city: $('#city').val(),
        state: $('#state').val(),
        zipcode: $('#zipcode').val(),
        email: email,
        step: 2
      }, 
      
      function(response){
        var status = JSON.parse(response);
        console.log(status);  
        
        if(status['response'] == "success"){
            
            $('#gtag-capture').html();
            $('#gtag-capture').html("<script>gtag('config', 'UA-99590284-2', {'page_path': '/free-trial-addy.html'});<\/script>");
            
            
            // $('.preloader').css('display', 'none');  
            $('#loadingBlackout').fadeOut();
            console.log('success');
            
            
            
            showHidePanels("personal_form", "area_code_form", "next", "bounceInRight", "bounceOutLeft");
            
        } else {
            $('#loadingBlackout').fadeOut();
            
            if(status['error'] == "duplicate_reg"){
              alert("A customer is already registered with this phone number. Please try again or contact support.");
            } else {
              alert("Something went wrong. Please try again or contact support.");   
            }
            
        }
        console.log("Sending data to API");
        
      });
    }
    });
});



$('#create_ccform_next').on('click', function(e){
	   $('#overlay_header').html('');
	   e.preventDefault();
	    showHidePanels("form_three", "form_four", "next", "bounceInRight", "bounceOutLeft");
	    
	    $("#create_form").validate({
	        errorLabelContainer: ".messageBox",
	        rules: {
	            phone_number: { required: true },
	            address: { required: true } ,
	            city: { required: true },
	            state: { required: true },
	            zipcode: { required: true }
	        },
	        messages: {
	            phone_number: "We need a phone number for validation.",
	            address: "Please enter your address.",
	            city: "Please enter your city.",
	            zipcode: "Please enter your zip code."
	        },
	  submitHandler: function() {
	    
	      
	    //   $('.preloader').css('display', 'block'); 
	      $('#loadingBlackout').fadeIn();
	      email = $('#email').val();
	      console.log("EMAIL: "+email);
	      
	    // set post params to send to api
	      $.post('https://ninjanumber.com/trial_checkout.php', {
	        phone_number: $('#phone_number').val(),
	        address: $('#address').val(),
	        address2: $('#address2').val(),
	        city: $('#city').val(),
	        state: $('#state').val(),
	        zipcode: $('#zipcode').val(),
	        email: email,
	        step: 2
	      }, 
	      
	      function(response){
	        var status = JSON.parse(response);
	        console.log(status);  
	        
	        if(status['response'] == "success"){
	            
	            $('#gtag-capture').html();
	            $('#gtag-capture').html("<script>gtag('config', 'UA-99590284-2', {'page_path': '/free-trial-addy.html'});<\/script>");
	            
	            
	            // $('.preloader').css('display', 'none');  
	            $('#loadingBlackout').fadeOut();
	            console.log('success');
	            
	            
	            
	            showHidePanels("personal_form", "area_code_form", "next", "bounceInRight", "bounceOutLeft");
	            
	        } else {
	            $('#loadingBlackout').fadeOut();
	            
	            if(status['error'] == "duplicate_reg"){
	              alert("A customer is already registered with this phone number. Please try again or contact support.");
	            } else {
	              alert("Something went wrong. Please try again or contact support.");   
	            }
	            
	        }
	        console.log("Sending data to API");
	        
	      });
	    }
	    });
	});


$('#verify_form_next').on('click', function(e){
	   $('#overlay_header').html('');
	   e.preventDefault();
	    showHidePanels("form_three", "form_five", "next", "bounceInRight", "bounceOutLeft");
	    
	    $("#verify_form").validate({
	        errorLabelContainer: ".messageBox",
	        rules: {
	            phone_number: { required: true },
	            address: { required: true } ,
	            city: { required: true },
	            state: { required: true },
	            zipcode: { required: true }
	        },
	        messages: {
	            phone_number: "We need a phone number for validation.",
	            address: "Please enter your address.",
	            city: "Please enter your city.",
	            zipcode: "Please enter your zip code."
	        },
	  submitHandler: function() {
	    
	      
	    //   $('.preloader').css('display', 'block'); 
	      $('#loadingBlackout').fadeIn();
	      email = $('#email').val();
	      console.log("EMAIL: "+email);
	      
	    // set post params to send to api
	      $.post('https://ninjanumber.com/trial_checkout.php', {
	        phone_number: $('#phone_number').val(),
	        address: $('#address').val(),
	        address2: $('#address2').val(),
	        city: $('#city').val(),
	        state: $('#state').val(),
	        zipcode: $('#zipcode').val(),
	        email: email,
	        step: 2
	      }, 
	      
	      function(response){
	        var status = JSON.parse(response);
	        console.log(status);  
	        
	        if(status['response'] == "success"){
	            
	            $('#gtag-capture').html();
	            $('#gtag-capture').html("<script>gtag('config', 'UA-99590284-2', {'page_path': '/free-trial-addy.html'});<\/script>");
	            
	            
	            // $('.preloader').css('display', 'none');  
	            $('#loadingBlackout').fadeOut();
	            console.log('success');
	            
	            
	            
	            showHidePanels("personal_form", "area_code_form", "next", "bounceInRight", "bounceOutLeft");
	            
	        } else {
	            $('#loadingBlackout').fadeOut();
	            
	            if(status['error'] == "duplicate_reg"){
	              alert("A customer is already registered with this phone number. Please try again or contact support.");
	            } else {
	              alert("Something went wrong. Please try again or contact support.");   
	            }
	            
	        }
	        console.log("Sending data to API");
	        
	      });
	    }
	    });
	});



$('#congrat_form_next').on('click', function(e){
	   $('#overlay_header').html('');
	   e.preventDefault();
	    showHidePanels("form_three", "form_six", "next", "bounceInRight", "bounceOutLeft");
	    
	    $("#congrat_form").validate({
	        errorLabelContainer: ".messageBox",
	        rules: {
	            phone_number: { required: true },
	            address: { required: true } ,
	            city: { required: true },
	            state: { required: true },
	            zipcode: { required: true }
	        },
	        messages: {
	            phone_number: "We need a phone number for validation.",
	            address: "Please enter your address.",
	            city: "Please enter your city.",
	            zipcode: "Please enter your zip code."
	        },
	  submitHandler: function() {
	    
	      
	    //   $('.preloader').css('display', 'block'); 
	      $('#loadingBlackout').fadeIn();
	      email = $('#email').val();
	      console.log("EMAIL: "+email);
	      
	    // set post params to send to api
	      $.post('https://ninjanumber.com/trial_checkout.php', {
	        phone_number: $('#phone_number').val(),
	        address: $('#address').val(),
	        address2: $('#address2').val(),
	        city: $('#city').val(),
	        state: $('#state').val(),
	        zipcode: $('#zipcode').val(),
	        email: email,
	        step: 2
	      }, 
	      
	      function(response){
	        var status = JSON.parse(response);
	        console.log(status);  
	        
	        if(status['response'] == "success"){
	            
	            $('#gtag-capture').html();
	            $('#gtag-capture').html("<script>gtag('config', 'UA-99590284-2', {'page_path': '/free-trial-addy.html'});<\/script>");
	            
	            
	            // $('.preloader').css('display', 'none');  
	            $('#loadingBlackout').fadeOut();
	            console.log('success');
	            
	            
	            
	            showHidePanels("personal_form", "area_code_form", "next", "bounceInRight", "bounceOutLeft");
	            
	        } else {
	            $('#loadingBlackout').fadeOut();
	            
	            if(status['error'] == "duplicate_reg"){
	              alert("A customer is already registered with this phone number. Please try again or contact support.");
	            } else {
	              alert("Something went wrong. Please try again or contact support.");   
	            }
	            
	        }
	        console.log("Sending data to API");
	        
	      });
	    }
	    });
	});



$('#transfer_noform_next').on('click', function(e){
	   $('#overlay_header').html('');
	   e.preventDefault();
	    showHidePanels("form_two", "form_seven", "next", "bounceInRight", "bounceOutLeft");
	    
	    $("#transfer_form").validate({
	        errorLabelContainer: ".messageBox",
	        rules: {
	            phone_number: { required: true },
	            address: { required: true } ,
	            city: { required: true },
	            state: { required: true },
	            zipcode: { required: true }
	        },
	        messages: {
	            phone_number: "We need a phone number for validation.",
	            address: "Please enter your address.",
	            city: "Please enter your city.",
	            zipcode: "Please enter your zip code."
	        },
	  submitHandler: function() {
	    
	      
	    //   $('.preloader').css('display', 'block'); 
	      $('#loadingBlackout').fadeIn();
	      email = $('#email').val();
	      console.log("EMAIL: "+email);
	      
	    // set post params to send to api
	      $.post('https://ninjanumber.com/trial_checkout.php', {
	        phone_number: $('#phone_number').val(),
	        address: $('#address').val(),
	        address2: $('#address2').val(),
	        city: $('#city').val(),
	        state: $('#state').val(),
	        zipcode: $('#zipcode').val(),
	        email: email,
	        step: 2
	      }, 
	      
	      function(response){
	        var status = JSON.parse(response);
	        console.log(status);  
	        
	        if(status['response'] == "success"){
	            
	            $('#gtag-capture').html();
	            $('#gtag-capture').html("<script>gtag('config', 'UA-99590284-2', {'page_path': '/free-trial-addy.html'});<\/script>");
	            
	            
	            // $('.preloader').css('display', 'none');  
	            $('#loadingBlackout').fadeOut();
	            console.log('success');
	            
	            
	            
	            showHidePanels("personal_form", "area_code_form", "next", "bounceInRight", "bounceOutLeft");
	            
	        } else {
	            $('#loadingBlackout').fadeOut();
	            
	            if(status['error'] == "duplicate_reg"){
	              alert("A customer is already registered with this phone number. Please try again or contact support.");
	            } else {
	              alert("Something went wrong. Please try again or contact support.");   
	            }
	            
	        }
	        console.log("Sending data to API");
	        
	      });
	    }
	    });
	});



    
    /* Open */
function openNav() {
    console.log("Inside Function openNav");
    $("#myNav").css("height", "100%");
}

/* Close */
function closeNav() {
    $("#myNav").css("height", "0%");
    showHidePanels(current_form, "basics_form", "next", "bounceInRight", "bounceOutLeft");
}

$('#area_code').on('keyup', function(){
    // limit($(this));
    
    var max_chars = 3;


        if($(this).length > max_chars) {
            $(this).val() = $(this).val().substr(0, max_chars);
        }

        if($(this).val().length == max_chars){
            console.log("Search");
        }
    
});




$('#openNav').on('click', function(){
    openNav();
});

var selected_number;

// Countdown Timer

function countdown(minutes) {
    var seconds = 60;
    var mins = minutes
    function tick() {
        var counter = document.getElementById("reserve_countdown");
        var current_minutes = mins-1
        seconds--;
        counter.innerHTML = current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds);
        if( seconds > 0 ) {
            var timeoutSeconds = setTimeout(tick, 1000);
        } else {
                if(mins > 1){
                    countdown(mins-1);           
                } else {
                    console.log("Countdown Finished");
                
                if(signup_status != 'complete'){
                    // closeNav();
                    location.reload();
                } else {
                    clearTimeout(timeoutSeconds);
                }  
            }
        }
    }
    tick();
}
 




$('.overlay').on('click', '.number', function(){
    
    // Show Button with selected number 
    $('#selected_number').css("display","block");
    $('#selected_number').html($(this).html());
    
    // Set select_number var
    selected_number = $(this).html();
    
    //Area Code Form Exit
    
    showHidePanels("area_code_form", "summary_form", "next", "bounceInRight", "bounceOutLeft");
    $('#done').html('');
    $('#done').append('Get My Ninja Number! <br>'+selected_number);
    
});

$('#done').on('click', function(){
    
    
    did = selected_number.replace(/\D+/g, "");
    
    $("#summary_form").validate({
        errorLabelContainer: ".messageBox",
        rules: {
            agree: { required: true }
        },
  messages: {
    agree: "You must agree to the terms of service to continue"
  },
  submitHandler: function() {
      var phone_number_1 = $('#phone_number').val();
      var phone_number = phone_number_1.replace(/\D+/g, "");
      console.log("Phone Number?: "+phone_number);
    // Try to reserve Number
    $.post('https://ninjanumber.com/trial_checkout.php', {
        phone_number: did,
        step: 4
        
    }, function(response){
                        
        var status = response;
        
                
        if(status == "failed"){
            alert('Number has been taken. Please try again');
        }else{
            reservationURL = status;
            

        // send verification code
        // $('.preloader').css('display', 'block'); 
        $('#loadingBlackout').fadeIn();
        // set post params to send to api
      
      $.post('https://ninjanumber.com/trial_checkout.php', {
          phone_number: $('#phone_number').val().replace(/\D+/g, ""),
          step: 5
      }, function(response){
        var status = JSON.parse(response);
        console.log(JSON.stringify(status));  
        
        if(status['response'] == "success"){
            // $('.preloader').css('display', 'none');
            $('#loadingBlackout').fadeOut();
            
            // Hide Summary Screen
            $('#summary_form').removeClass('animated bounceInRight');
            $('#summary_form').addClass('animated bounceOutLeft');
                            
            // Show verification screen
            $('#overlay_verify_form').css('display','block');
            $('#overlay_verify_form').addClass('animated bounceInRight');
            console.log('Success!');
                            
            // Start Timer
            countdown(10);
            
            $('#gtag-capture').html();
            $('#gtag-capture').html("<script>gtag('config', 'UA-99590284-2', {'page_path': '/free-trial-verify.html'});<\/script>");
            
        } else {
            console.log("There was a problem sending verification code");
        }
        
        
      });
                            
     }
    });
    
    }
    });
    
});

$('#step_1.step.row').on('click', '#selected_number', function(){
    console.log('SELECTED NUMBER');
    openNav();
});

$('#account_info').on('click', function(){

   $('#overlay_header').html('Enter Your Personal Information');
   $('#basics_form').css('display','block');
   $('#basics_form').addClass('animated bounceInLeft');
   
   $('#gtag-capture').html();
   $('#gtag-capture').html("<script>gtag('config', 'UA-99590284-2', {'page_path': '/free-trial'});<\/script>");
   
   openNav(); 
});


    
    function formatPhoneNumber(s) {
        var s2 = (""+s).replace(/\D/g, '');
        var m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
        return (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
    }
    
    $("#verify_code").keyup(function(){
        var phone_number = $('#phone_number').val();
        var phone = phone_number.replace(/\D+/g, "");
        did = selected_number.replace(/\D+/g, "");
        
        if($(this).val().length == 4){
            console.log('Success: '+phone);
            
            // $('.preloader').css('display', 'block');
            $('#loadingBlackout').fadeIn();
            
            // Call verification api
            $.post('https://ninjanumber.com/trial_checkout.php', {
                phone_number: phone,
                verification_code: $("#verify_code").val(),
                step: 6
            }, function(response){
                var status = JSON.parse(response);
                console.log(JSON.stringify(status));
                
                if(status['success'] == true){
                    
                    // get reservation id
                    var reservationID = reservationURL.substr(reservationURL.lastIndexOf('/') + 1);
                    console.log("reservation id: "+reservationID);
                    
                    // Order number
                    
                     $.post('https://ninjanumber.com/trial_checkout.php',{
                         reservation_id: reservationID,
                         phone_number: did,
                         step: 7
                     }, function(response){
                        var status = JSON.parse(response);
                        
                        console.log("Ordered Reserved Number?: "+status[0]);
                        
                        if(status[0] == "RECEIVED" || status[0] == "COMPLETE"){
                            
                            var first_name = $('#first_name').val();
                            var last_name  = $('#last_name').val();
                            var email      = $('#email').val();
                            var reference  = $('#how').val();
                            var company    = $('#company').val();
                            var shippingaddress1 = $('#address').val();
                            var shippingaddress2 = $('#address2').val();
                            var shippingcity = $('#city').val();
                            var shippingstate = $('#state').val();
                            var shippingpostalcode = $('#zipcode').val();
                            
                            // processOrder
                        $.post('https://ninjanumber.com/trial_checkout.php',{
                            reservation_id: reservationID,
                            phone: phone,
                            did_number: did,
                            firstname: first_name,
                            lastname: last_name,
                            email: email,
                            companyname: company,
                            shippingaddress1: shippingaddress1,
                            shippingaddress2: shippingaddress2,
                            shippingcity: shippingcity,
                            shippingstate: shippingstate,
                            shippingpostalcode: shippingpostalcode,
                            id: scrm_id,
                            step: 8
                        }, function(response){
                            var status = JSON.parse(response);
                            console.log("Final  Step?: "+status);
                            
                            // $('.preloader').css('display', 'none');
                            $('#loadingBlackout').fadeOut();
                            
                            // Hide Verify Screen
                            $('#overlay_verify_form').removeClass('animated bounceInRight');
                            $('#overlay_verify_form').addClass('animated bounceOutLeft');
                
                            // Show Congrats Screen
                            $('#congrats_form').css('display','block');
                            $('#congrats_form').addClass('animated bounceInRight');
                            signup_status = "complete";
                            
                            $('#gtag-capture').html();
                            $('#gtag-capture').html("<script>gtag('config', 'UA-99590284-2', {'page_path': '/free-trial-ty.html'});<\/script><script>gtag('event', 'conversion', {'send_to': 'AW-825361347/RbtsCIKZ9n4Qw4fIiQM'});<\/script>");
                            
                        });
                            
                        } else {
                            alert('There was a problem assigning number. Please try again.');
                            $('#loadingBlackout').fadeOut();
                        }
                     });
                    
                } else {
                    alert("Verification Code Incorrect. Please try again");
                    $('#loadingBlackout').fadeOut();
                }
                
            });
        }
    });

    console.log("hello");
    $(".option").on('click',function(){
        $(this).hide();
        console.log($(this));
    });
       
 $("#area_code").keyup(function(){
        if($(this).val().length > 2){
            // $('.preloader').css('display', 'block'); 
            $('#loadingBlackout').fadeIn();
            
            $('#number_select').html('');
            var areaCode = $(this).val();
            $('#select_number').children('option').remove();

            $.ajax({
            url: "trial_checkout.php",            
            type: 'POST',
            dataType: 'json',
            data: {
                area_code: $(this).val(),
                step: 3
             },
            success: function (response) {
                // $('.preloader').css('display', 'none');  
                $('#loadingBlackout').fadeOut();
                console.log(response);

                if(jQuery.isEmptyObject(response)){
                    $('#select_number').html('');
                    $('.messageBox').css('display', 'block');
                    $('.messageBox').html('We couldn\'t find a number for that area code.');
                    
                    // $('#select_number').append('<option value="" >No '+areaCode+' numbers available</option>');
                    $('#select_number').append('No '+areaCode+' numbers available');
                }else{
                    // openNav();
                    $('.messageBox').css('display', 'none');
                 // $('#select_number').append('<div id="number_container">');
                 // $('#number_select').append('<div class="option"><input type="radio" name="number_option" value="number_option" id="number_option"></div>');
                    console.log(Array.isArray(response['TelephoneNumber']));
                   //$('#number_select').append('<div class="option">');
                    if(Array.isArray(response['TelephoneNumber'])){
                    
                        for(var i = 0; i < response['TelephoneNumber'].length; i++) { 
                                	
                        console.log(response['TelephoneNumber'][i]);
                         //$('#select_number').append('<option value="'+response['TelephoneNumber'][i]+'">'+formatPhoneNumber(response['TelephoneNumber'][i])+'</option>');
                        $('#number_select').append('<div class="option"><label class="number_option" for="number_option">'+formatPhoneNumber(response["TelephoneNumber"][i])+'</label></div>');
                        //$('#number_select').append('<label for="number_option">'+formatPhoneNumber(response["TelephoneNumber"][i])+'</label>');
                    }
                        $('#number_select').append('<div class="more cf">More</div>');
                    } 
                    else {
                    	 $('#number_select').append('<div class="option"><label class="number_option" for="number_option">'+formatPhoneNumber(response["TelephoneNumber"])+'</label></div>');
                     
                    }
                    
                    $('#gtag-capture').html();
                    $('#gtag-capture').html("<script>gtag('config', 'UA-99590284-2', {'page_path': '/free-trial-number.html'});<\/script>");
                    
                 // $('#number_select').append('</div>');
                    $('#number_select').addClass('animated bounceInRight');
                   
                }
            },
            error: function(error){
                console.log("Cannot get data "+JSON.stringify(error));
            }
        });
           
        }
        
    });


 $("#city").keyup(function(){
     if($('#city').val()){
         // $('.preloader').css('display', 'block'); 
         $('#loadingBlackout').fadeIn();
         
         $('#number_select').html('');
         var strcity = $('#city').val();
         var splitStr = strcity.split(',');
         var partStr = ''; 
        for (var i = 0; i < splitStr.length; i++) {
         splitStr[i] = splitStr[i].replace(/^\s+|\s+$/g, '') ; 
        }
         var string_city = splitStr[0];
         var string_state = splitStr[1]; 
        // var state = $(this).val();
         $('#select_number').children('option').remove();

         $.ajax({
         url: "trial_checkout.php",            
         type: 'POST',
         dataType: 'json',
         data: {
             city: string_city,
             state: string_state,
             step: 3
          },
         success: function (response) {
             // $('.preloader').css('display', 'none');  
             $('#loadingBlackout').fadeOut();
             console.log(response);

             if(jQuery.isEmptyObject(response)){
                 $('#select_number').html('');
                 $('.messageBox').css('display', 'block');
                 $('.messageBox').html('We couldn\'t find a number for that area code.');
                 
                 // $('#select_number').append('<option value="" >No '+areaCode+' numbers available</option>');
                 $('#select_number').append('No '+city+' numbers available');
             }else{
                 // openNav();
                 $('.messageBox').css('display', 'none');
                // $('#number_select').append('<div class="option"><input type="radio" name="number_option" value="number_option" id="number_option"><label for="number_option"></label></div>');
                 console.log(Array.isArray(response['TelephoneNumber']));
                 
                 if(Array.isArray(response['TelephoneNumber'])){
                     for(var i = 0; i < response['TelephoneNumber'].length; i++) {
                     console.log(response['TelephoneNumber'][i]);
                      //$('#select_number').append('<option value="'+response['TelephoneNumber'][i]+'">'+formatPhoneNumber(response['TelephoneNumber'][i])+'</option>');
                     $('#number_select').append('<div class="option">'+formatPhoneNumber(response["TelephoneNumber"][i])+'</div>');
                     //$('#number_select').append('<label for="number_option">'+formatPhoneNumber(response["TelephoneNumber"][i])+'</label>');
                 }
                     $('#number_select').append('<div class="more cf">More</div>');
                 } else {
                 	 $('#number_select').append('<div class="option">'+formatPhoneNumber(response["TelephoneNumber"])+'</div>');
                     //$('#number_select').append('<input type="text" value="'+formatPhoneNumber(response["TelephoneNumber"])+'">');
                 	 //$('#number_select').append('<label for="number_option">'+formatPhoneNumber(response["TelephoneNumber"][i])+'</label>');
                 }
                 
                 $('#gtag-capture').html();
                 $('#gtag-capture').html("<script>gtag('config', 'UA-99590284-2', {'page_path': '/free-trial-number.html'});<\/script>");
                 
                $('#number_select').append('</div>');
                 $('#number_select').addClass('animated bounceInRight');
                
             }
         },
         error: function(error){
             console.log("Cannot get data "+JSON.stringify(error));
         }
     });
        
     }
     
 });





//  var addressPicker = new AddressPicker();
 
//  $('#city').typeahead(null, {
//    displayKey: 'description',
//    source: addressPicker.ttAdapter()
//  });

 


 
});








</script>




</head>
	








<body>
<div class="wrap">

<header>
	<svg class="logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 87"><path class="orange-fill" d="M64.8 0c11.9 0 22.7 4.8 30.5 12.6 7.8 7.8 12.6 18.6 12.6 30.5 0 11.9-4.8 22.7-12.6 30.5-7.8 7.8-18.6 12.6-30.5 12.6-11.9 0-22.7-4.8-30.5-12.6-7.8-7.8-12.6-18.6-12.6-30.5 0-11.9 4.8-22.7 12.6-30.5C42.1 4.8 52.9 0 64.8 0M117.4 18.7h5.9l13.6 17.8V18.7h6.3v29h-5.5l-14-18.5v18.5h-6.3zM150.4 18.7h6.4v29h-6.4zM164 18.7h5.9l13.6 17.8V18.7h6.3v29h-5.4l-14.1-18.5v18.5H164zM203.3 48.1c-2.3 0-4.2-.4-5.7-1.3s-2.8-1.9-3.8-3.1l4-4.5c.8.9 1.7 1.6 2.5 2.1s1.8.8 2.8.8c1.2 0 2.2-.4 2.9-1.1.7-.8 1-2 1-3.7V18.7h6.5v19c0 1.7-.2 3.2-.7 4.5-.5 1.3-1.1 2.3-2 3.2-.9.9-2 1.6-3.2 2-1.3.4-2.7.7-4.3.7M230 18.5h5.9l12.4 29.2h-6.7l-2.6-6.5h-12.3l-2.6 6.5h-6.5L230 18.5zm6.7 17l-3.8-9.4-3.8 9.4h7.6z"/><path class="purple-fill" d="M117.4 55.7h5.9l13.6 17.8V55.7h6.3v29h-5.5l-14-18.4v18.4h-6.3zM162.2 85.1c-3.9 0-7-1.1-9.2-3.2-2.2-2.2-3.4-5.4-3.4-9.6V55.7h6.4v16.4c0 2.4.6 4.2 1.7 5.4 1.1 1.2 2.6 1.8 4.6 1.8 2 0 3.5-.6 4.6-1.7 1.1-1.2 1.7-2.9 1.7-5.2V55.7h6.4V72c0 2.2-.3 4.1-.9 5.8-.6 1.6-1.4 3-2.5 4.1-1.1 1.1-2.4 1.9-4 2.4-1.7.6-3.5.8-5.4.8M181.4 55.7h6.9l7.6 12.3 7.6-12.3h6.9v29h-6.3V65.8l-8.2 12.3h-.1l-8.1-12.2v18.8h-6.3zM217.3 55.7h13.4c3.3 0 5.9.9 7.6 2.6 1.3 1.3 2 3 2 4.9v.1c0 .8-.1 1.6-.3 2.2-.2.6-.5 1.2-.8 1.7-.3.5-.7 1-1.2 1.3-.5.4-.9.7-1.5 1 1.7.6 3 1.5 4 2.6s1.4 2.6 1.4 4.5v.1c0 1.3-.3 2.5-.8 3.5s-1.2 1.8-2.2 2.5c-1 .7-2.1 1.2-3.4 1.5-1.3.3-2.8.5-4.4.5h-13.8v-29zm12.1 11.7c1.4 0 2.5-.2 3.4-.7.8-.5 1.2-1.3 1.2-2.3v-.1c0-1-.4-1.7-1.1-2.2-.7-.5-1.8-.8-3.1-.8h-6.3v6.1h5.9zm1.7 11.7c1.4 0 2.5-.3 3.3-.8.8-.5 1.2-1.3 1.2-2.4v-.1c0-1-.4-1.7-1.1-2.3-.7-.6-1.9-.8-3.6-.8h-7.4v6.4h7.6zM247.2 55.7H269v5.7h-15.5v5.9h13.7v5.6h-13.7V79h15.7v5.7h-22zM275 55.7h13.2c3.7 0 6.5 1 8.4 2.9 1.7 1.7 2.5 3.9 2.5 6.6v.1c0 2.3-.6 4.3-1.7 5.7-1.1 1.5-2.6 2.6-4.5 3.2l7.1 10.3h-7.5l-6.2-9.3h-5v9.3H275V55.7zm12.8 14.1c1.6 0 2.8-.4 3.6-1.1.8-.7 1.3-1.7 1.3-3v-.1c0-1.4-.4-2.4-1.3-3.1-.9-.7-2.1-1-3.7-1h-6.3v8.3h6.4zM92.4 67.8c5.9-6.6 9.4-15.2 9.4-24.7 0-10.2-4.1-19.5-10.8-26.2-6.7-6.6-16-10.8-26.2-10.8S45.3 10.3 38.6 17c-3.1 3.1-5.7 6.8-7.5 10.8-2.9-4.7-7.1-7.3-13.9-9.2 1.7 6.2 4 10.2 7.9 13-8.4-2.5-15.6-1.1-25.1 4.2 10.6 6 18.4 7 28 3.1-.2 1.4-.2 2.8-.2 4.2 0 10.2 4.1 19.5 10.8 26.2 6.7 6.7 16 10.8 26.2 10.8 6.5 0 12.7-1.7 18-4.7l12.8 10.3-3.2-17.9z"/><path class="white-fill" d="M88.7 41.7c-1.2-8.9-12.2-12-23.9-12s-22.7 3.1-23.9 12c-.8 6 3.3 15.5 19 11.8 3.2-.7 6.6-.7 9.8 0 15.8 3.8 19.8-5.8 19-11.8"/><path class="purple-fill" d="M58.5 42.2c-.1-.1-1.3-1.4-2.9-1.9-1.5-.5-3.3-.5-4.8 0-1.6.5-2.7 1.7-2.8 1.8-.4.5-.6 1.1-.5 1.7-.1.6.1 1.2.5 1.7.1.1 1.3 1.3 2.8 1.8 1.5.5 3.3.5 4.8 0 1.6-.5 2.8-1.8 2.9-1.9.4-.4.5-1 .5-1.5v-.1-.1c.1-.5-.1-1.1-.5-1.5M81.6 42.2c-.1-.1-1.3-1.3-2.8-1.8-1.5-.5-3.3-.5-4.8 0-1.6.5-2.8 1.8-2.9 1.9-.4.4-.5 1-.5 1.5v.2c0 .6.1 1.1.5 1.5.1.1 1.3 1.4 2.9 1.9 1.5.5 3.3.5 4.8 0 1.6-.5 2.7-1.7 2.8-1.8.4-.5.6-1.1.5-1.7.1-.7 0-1.3-.5-1.7"/></svg>
	<h1>Try Ninja Number <em>for Free</em></h1>
	<p>No contracts. No commitment. And you decide when we change your credit card.</p>

	
</header>

<div id="form_one">
<div class="main initial">
<div class="sales logos">
		<h2>Trusted By Over <em>75,000 Businesses!</em></h2>
		<div class="client-logos">
			<img src="https://ninjanumber.com/wp-content/uploads/2018/05/mcdonalds.svg" alt="McDonalds's">
			<img src="https://ninjanumber.com/wp-content/uploads/2018/05/dell.svg" alt="Dell Computers">
			<img src="https://ninjanumber.com/wp-content/uploads/2018/05/delta.svg" alt="Delta Airlines">
			<img src="https://ninjanumber.com/wp-content/uploads/2018/05/dunkin-donuts.svg" alt="Dunkin' Donuts">
			<img src="https://ninjanumber.com/wp-content/uploads/2018/05/adobe.svg" alt="Adobe">
			<img src="https://ninjanumber.com/wp-content/uploads/2018/05/hyundai.svg" alt="Hyundai">
		</div>			
	</div>
<div class="form">
<form id="basics-form" class="box">
<h3>First tell us a little about yourself:</h3>
<div class="group">
<div class="fields cf">

<div class="field">
<label for="first_name">First Name</label>
<input id="first_name" type="text" name="first_name" required/>
</div>

<div class="field">
<label for="last_name">Last Name</label>
<input id="last_name" type="text" name="last_name" required/>
</div>

</div>
<div class="fields cf">

<div class="field wide">
<label for="email">Email Address*</label>
<input id="email" type="email" name="email" required/>
</div>

</div>
</div>
<div class="buttons cf">
<button id="basics_form_next" class="next active">Get Started</button>
</div>
</form>
</div>
</div>
</div>

<div id="form_two">
<div class="main choose-number">
	<div class="sales testimonial">
		<blockquote>
			"Thanks to Ninja Number, we now find that calls are routed to appropriate departments without bogging down our office manager. Employees working in the field are reached without delay thanks to call forwarding options, and overall customer satisfaction has increased through this service."
			<cite>
				<img SRC="https://ninjanumber.com/wp-content/uploads/2018/05/temp-headshot.jpg">
				<strong>Chris Casual</strong>
				Athena Enterprises LLC
			</cite>
		</blockquote>
	</div>
<div class="form">
<form id="personal_form" class="box" action="/" method="post" autocomplete="off">
			<h3>
				Choose Your Number
				<small class="step-counter">
					Step <span class="step-current">X</span> of <span class="step-total">X</span>
				</small>
			</h3>
			<p>It doesn�t matter where you�re located. Choose any number you want!</p>	
			<div class="tabs">
				<div class="new-number active">Choose a New Number</div>
				<div id="transfer_noform_next" class="transfer-number">Transfer Existing Number</div>				
			</div>
			<div class="group search-numbers">
				<h4>Search Numbers</h4>
				<div class="fields cf">

					<div class="field">
						<label for="area_code">Area Code</label>
						<input id="area_code" type="text" name="area_code"/>
					</div>
					
					<div class="or"><em>OR</em></div>
					
					<div class="field">                      
						<label for="city">City</label>
						<input id="city" type="text" name="city" placeholder="City/Town, State"/>
					    	
					</div>
				

				</div>
			</div>  
			<div class="group number-select">
			
				<h4>Choose a Number</h4>

				<div id="number_select" class="options cf">				
				<div class="messageBox error"></div>
				

				</div>

			</div>   			
			<div class="buttons">
				<button id="personal_form_back" class="previous">Previous</button>				  				  
				<button id="personal_form_next" class="next active">Next</button>
			</div>   			
		</form>
	
</div>
</div>
</div>
</div>

<div id="form_three">
<div class="main credit">
	<div class="sales partner">
		<div class="voice-nation cf">
			<!-- <span>Partner of</span> <img src="voicenation.png" alt="VoiceNation"> -->
		</div>
		<div class="open-answer cf">
			<span>Creator of open source software</span>
			<!-- <img src="openanswer.png" alt="OpenAnswer"> -->
		</div>
	</div>
	<div class="form">

		<form class="box" action="/">
			<h3>
				Create Your Account
				<small class="step-counter">
					Step <span class="step-current">X</span> of <span class="step-total">X</span>
				</small>
			</h3>

			<div class="group address">
				<div class="fields cf">

					<div class="field wide">
						<label for="address1">Address</label>
						<input id="address1" type="text" name="address1"/>
					</div>

				</div>
				
				<div class="fields three cf">
					
					<div class="field half">
						<label for="address-city">City</label>
						<input id="address-city" type="text" name="address-city"/>
					</div>
					
					<div class="field">
						<label for="address-state">State</label>
						<input id="address-state" type="text" name="address-state"/>
					</div>							
					
					<div class="field">
						<label for="address-zip">Zip Code</label>
						<input id="address-zip" type="phone" name="address-zip"/>
					</div>					

				</div>	
				
				<div class="fields cf">

					<div class="field phone wide">
						<label for="phone">Phone Number</label>
						<input id="phone" type="phone" name="phone"/>
					</div>

				</div>				
			</div>  
			
			<div class="group credit-card">
				<h4>Payment Information</h4>
				<div class="fields">

					<div class="field wide">
						<label for="credit-card-number">Credit Card Number</label>
						<input id="credit-card-number" type="text" name="credit-card-number"/>
					</div>

				</div>
				
				<div class="fields credit-details">
					
					<div class="field">
						<label for="credit-card-expiration-month">Expiration</label>
						<input id="credit-card-expiration-month" type="phone" name="credit-card-expiration-month" placeholder="Month" />
					</div>
					
					<div class="field">
						<label for="credit-card-expiration-year"></label>
						<input id="credit-card-expiration-year" type="phone" name="credit-card-expiration-year" placeholder="Year" />
					</div>							
					
					<div class="field">
						<label for="credit-card-cvv">CVV</label>
						<input id="credit-card-cvv" type="phone" name="credit-card-cvv" />
					</div>					

				</div>	

			</div>  	
			
			<div class="group checkbox agreement">
				<div class="options cf">

					<div class="option">
						<input type="checkbox" name="checkbox-grouping-test" value="option2" id="checkbox-grouping-test_option2" aria-required="true">
						<label for="checkbox-grouping-test_option2">
							<span class="selector"></span>
							<em>I agree to the <a href="/terms">Terms and Conditions</a></em>
						</label>
					</div>

				</div>
			</div>

			<div class="buttons">
			    <button id="form_three_back" type="submit" class="previous">Previous</button>								  				  
				<button id="congrat_form_next" class="submit active">
					<svg class="padlock" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 142.2"><path d="M88 62.77c.14-9 .37-26.09.2-28.84C87.1 16.45 72 1.62 53.91.16 52.77.07 51.63 0 50.53 0a38.42 38.42 0 0 0-25.92 9.18 36.39 36.39 0 0 0-12.76 24.21c-.2 2.54-.13 21-.11 29.64A46.92 46.92 0 0 0 0 94c0 26.6 22.39 48.16 50 48.16s50-21.52 50-48.16a47 47 0 0 0-12-31.23zm-37.75 46.61a10.61 10.61 0 0 1-6.1-19.29 7.5 7.5 0 1 1 12.21 0 10.61 10.61 0 0 1-6.11 19.29zM50 45.87a51.28 51.28 0 0 0-22 4.92c.28-10 1.06-21 3.16-24.45a21.72 21.72 0 0 1 18.27-10.63h.8a21.6 21.6 0 0 1 18.46 10.42C71 29.8 71.77 40.55 72 50.77a51.24 51.24 0 0 0-22-4.9z"/></svg>
					Get My Ninja Number
				</button>
				<em>Secured by Verisign</em>
				
			</div>   			
		</form>
	
	</div>	
</div>
</div>	

<div id="form_six">
<div class="main transfer one-col">
	<div class="form">
	<form action="https://dashboard.ninjanumber.com" method="post">
		<div class="box">
			<h3>Congatulations!</h3>
			<p>Your Ninja Number is now active and ready to be used. Check your email for a summary of your service and login credentials to your Ninja Number dashboard. Accessing your dashboard will allow you to view your messages, switch between applications, and upgrade to a paid Ninja Number plan when you�re ready!</p>
			<div class="buttons cf">
				<button type="submit" class="next active">Go to my Dashboard</button>
			</div>   
		</div>
		</form>
	</div>	
</div>
</div>	

	
<div id="form_seven">
<div class="main transfer one-col">
	<div class="form">
		<form class="box" action="/">
			<h3>Transfer Your existing Number</h3>
			<p>Yes, you can transfer your number to Ninja Number! Continue filling out the form and a member of our customer support staff will reach out to help you move your number.</p>
			<div class="buttons cf">
				<button type="submit" class="next active">Continue</button>
			</div>   
		</form>
	</div>	
</div>
</div>	

	
<footer>
	<div class="secured">Secured by Verisign</div>
	<a HREF="https://ninjanumber.com/terms">Terms & Conditions</a> <a HREF="https://ninjanumber.com/privacy">Privacy Policy</a>    
</footer>

</body>
</html>
	
