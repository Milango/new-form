<?php 

// If getting form data
$request = $_POST;
$request['acc_num'] = "5003012";
$request['checkout_type'] = "trial";
$request['sku'] = "NNFREE";

// If getting raw data use: $request = file_get_contents("php://input"); instead

switch($request['step']){
    case 1: 
        $action = "saveTrialUserContact";
        $uri    = "https://api.nextpbx.com/ninjanumber.php"; 
        break;
    case 2: 
        $action = "updateTrialUser";
        $uri    = "https://api.nextpbx.com/ninjanumber.php"; 
        break;
    case 3:
        $action = "getAvailableNumbers";
       // $uri    = "https://api.nextpbx.com/bandwidth.php";
        $uri    = "https://api.nextpbx.com/bandwidth-M.php";
        $request['vendor'] = 'bandwidth';
        $request['quantity'] = 24;        
        break;
    case 4:
        $action = "reserveNumber";
        $uri    = "https://api.nextpbx.com/bandwidth.php";
        break;
    case 5:
        $action = "sendSMSValCode";
        $uri    = "https://api.nextpbx.com/ninjanumber.php";
        $request['country_code'] = "1";
        $request['method'] = "sms";
        $request['vendor'] = "twilio";
        break;
    case 6:
        $action = "verifyTrialCode";
        $uri    = "https://api.nextpbx.com/ninjanumber.php";
        $request['country_code'] = "1";        
        $request['vendor'] = "twilio"; 
        break;
    case 7:
        $action = "orderNumber";
        $uri    = "https://api.nextpbx.com/bandwidth.php";
        $request['country_code'] = "1";        
        $request['vendor'] = "twilio"; 
        break;
    case 8:
        $action = "processWebFreeTrial";
        $uri    = "http://api.nextpbx.com/nextpbx.php";
        $request['planname'] = "nnfreetrial-7day";
        break;
    
}



$request['apiuser'] = 'checkoutpage';
$request['apisecret'] = '5Abul0us';
$request['apicompany'] = '1';
$request['action'] = $action;

$body= json_encode($request);         
// echo json_encode($request); exit();
$ch = curl_init();

// Set curl options
curl_setopt($ch, CURLOPT_URL, $uri);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

// Send the request
$response = curl_exec($ch);
// Handle errors
if ($response === false) {
    $this->errorcode = curl_errno($ch);
    $this->errormessage = curl_error($ch);
    curl_close($ch);
    print_r($this->errormessage);
    return false;
}

$statusCode  = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);

//attempt to decode the response into an associative array
// $decoded = json_decode($response,true);
echo $response;